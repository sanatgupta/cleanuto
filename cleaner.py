#░█████╗░██╗░░░░░███████╗░█████╗░███╗░░██╗██╗░░░██╗████████╗░█████╗░
#██╔══██╗██║░░░░░██╔════╝██╔══██╗████╗░██║██║░░░██║╚══██╔══╝██╔══██╗
#██║░░╚═╝██║░░░░░█████╗░░███████║██╔██╗██║██║░░░██║░░░██║░░░██║░░██║
#██║░░██╗██║░░░░░██╔══╝░░██╔══██║██║╚████║██║░░░██║░░░██║░░░██║░░██║
#╚█████╔╝███████╗███████╗██║░░██║██║░╚███║╚██████╔╝░░░██║░░░╚█████╔╝
#░╚════╝░╚══════╝╚══════╝╚═╝░░╚═╝╚═╝░░╚══╝░╚═════╝░░░░╚═╝░░░░╚════╝░ the lazy file cleaner



import os
import shutil
import time
def remove_folder(path):

	# removing the folder
	if not shutil.rmtree(path):

		# success message
		print(f"{path} is removed successfully")

	else:

		# failure message
		print(f"Unable to delete the {path}")


def remove_file(path):
    # removing the folder
	if not os.remove(path):

		# success message
		print(f"{path} is cleaned")

	else:

		# failure message
		print(f"Unable to clean the {path}")


def get_file_folder_age(path):

	# getting ctime of the file/folder
	# time will be in seconds
	ctime = os.stat(path).st_ctime

	# returning the time
	return ctime



def main():
    print("Starting cleanuto service...")
    time.sleep(3)
    print("initialized..")
    print("successfully running....")
    # just for style and theatrics
    if os.name == 'posix':
        _ = os.system('clear')
    else:
      # for windows platfrom
      _ = os.system('cls')
    #printing cleanuto logo
    print("""

░█████╗░██╗░░░░░███████╗░█████╗░███╗░░██╗██╗░░░██╗████████╗░█████╗░
██╔══██╗██║░░░░░██╔════╝██╔══██╗████╗░██║██║░░░██║╚══██╔══╝██╔══██╗
██║░░╚═╝██║░░░░░█████╗░░███████║██╔██╗██║██║░░░██║░░░██║░░░██║░░██║
██║░░██╗██║░░░░░██╔══╝░░██╔══██║██║╚████║██║░░░██║░░░██║░░░██║░░██║
╚█████╔╝███████╗███████╗██║░░██║██║░╚███║╚██████╔╝░░░██║░░░╚█████╔╝
░╚════╝░╚══════╝╚══════╝╚═╝░░╚═╝╚═╝░░╚══╝░╚═════╝░░░░╚═╝░░░░╚════╝░ the lazy file cleaner


    """)

    path = input("Please enter the file path: ")
    days = 30
    #days into minutes
    seconds = time.time() - (days * 24 * 60 * 60)
    #checking if the path exists
    if os.path.exists(path):

        for root, dirs, files in os.walk(path):
            if seconds >= get_file_folder_age(root):
                #removing the root folder
                remove_folder(root)
                break
            else:
                for folder in dirs:

                    folder_path = os.path.join(root, folder)

                    if seconds >= get_file_folder_age(folder_path):
                        remove_folder(folder_path)
                

                for file in files:

                    #file path
                    file_path = os.path.join(root, file)

                    if seconds >= get_file_folder_age(file_path):
                        remove_file(file_path)
        else:
            #if the whole folder is old then delete it
            if seconds >= get_file_folder_age(file_path):
                remove_file(file_path)
            

    #if the path doesn't exist then  
    else:

        print(f'"{path}" path was not found! kindly enter the correct path.')

 



if __name__ == '__main__':
	main()