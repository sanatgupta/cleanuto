░█████╗░██╗░░░░░███████╗░█████╗░███╗░░██╗██╗░░░██╗████████╗░█████╗░
██╔══██╗██║░░░░░██╔════╝██╔══██╗████╗░██║██║░░░██║╚══██╔══╝██╔══██╗
██║░░╚═╝██║░░░░░█████╗░░███████║██╔██╗██║██║░░░██║░░░██║░░░██║░░██║
██║░░██╗██║░░░░░██╔══╝░░██╔══██║██║╚████║██║░░░██║░░░██║░░░██║░░██║
╚█████╔╝███████╗███████╗██║░░██║██║░╚███║╚██████╔╝░░░██║░░░╚█████╔╝
░╚════╝░╚══════╝╚══════╝╚═╝░░╚═╝╚═╝░░╚══╝░╚═════╝░░░░╚═╝░░░░╚════╝░ the lazy file cleaner


A file cleaner that automatically cleans files that are more than 30 days old you
can increase the days by editing

days = custom

Please don't reuse this code without crediting @sanatg

follow me on github if you like my programming.

